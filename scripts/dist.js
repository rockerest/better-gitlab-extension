import { mkdirSync, readFileSync, writeFileSync } from "fs";

var cwd = process.cwd();

function copyFiles(){
	var css = {
		"src/todo.css": "todo.css",
		"node_modules/quill/dist/quill.bubble.css": "vendor/quill.css"
	};

	mkdirSync( `${cwd}/dist/styles/vendor`, {
		"recursive": true
	} );

	Object
		.entries( css )
		.forEach( ( [ relPath, dest ] ) => {
			let content = readFileSync( `${cwd}/${relPath}`, "utf8" );

			writeFileSync( `${cwd}/dist/styles/${dest}`, content );
		} );
}

copyFiles();