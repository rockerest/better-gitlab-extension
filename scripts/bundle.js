import { rollup } from "rollup";
import { nodeResolve as resolve } from "@rollup/plugin-node-resolve";
import cjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";

var cwd = process.cwd();
var input = {
	"plugins": [
		resolve( {
			"preferBuiltins": true
		} ),
		cjs(),
		json()
	]
};

var modules = [
	"./src/todo.js"
];

async function bundle( path, name ){
	var compiled = await rollup( { ...input, "input": path } );

	await compiled.write( {
		"format": "esm",
		"file": `${cwd}/dist/js/${name}.js`
	} );
}

modules.forEach( async ( path ) => {
	var parts = path.split( "/" );
	var name = parts.pop().replace( ".js", "" );

	await bundle( path, name );
} );