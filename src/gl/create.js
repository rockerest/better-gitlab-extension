import { getTools } from "../common/Dom.js";

export function createScratchpad(){
	var { qs, ce } = getTools();

	var contentRegion = qs( "#content-body.content" );
	var tabs = contentRegion.querySelector( ".top-area" );
	var allChildren = Array.from( contentRegion.children );
	var leaveAloneThrough = allChildren.indexOf( tabs );

	var splitContainer = ce( "div" );
	var glTodos = ce( "div" );
	var personalChecklist = ce( "div" );
	var toggle = ce( "div" );

	personalChecklist.setAttribute( "id", "gitlab-personal-checklist" );

	toggle.setAttribute( "id", "gitlab-personal-checklist-toggle" );
	toggle.textContent = "📝";

	splitContainer.appendChild( personalChecklist );
	splitContainer.appendChild( toggle );
	splitContainer.appendChild( glTodos );

	contentRegion.appendChild( splitContainer );

	allChildren.forEach( ( child, i ) => {
	if( i > leaveAloneThrough ){
		glTodos.appendChild( child );
	}
	} );

	splitContainer.setAttribute( "id", "gitlab-personal-checklist-container" );
	splitContainer.classList.add( "has-scratchpad" );

	return { personalChecklist, toggle };
}