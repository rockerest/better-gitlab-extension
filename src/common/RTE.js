import Quill from "quill";

function guaranteeOps( line ){
	if( line.ops.length == 0 ){
		line.ops.push( { "insert": "\n" } );
	}

	return line;
}

function getIndexesOfInserts( line ){
	var bools = line.ops.map( ( op ) => Boolean( op.insert ) );
	var inserts = [ bools.indexOf( true ), bools.lastIndexOf( true ) ];

	return inserts;
}

function getLastInsertOp( line ){
	var line = guaranteeOps( line );
	var op = line.ops[ getIndexesOfInserts( line )[ 1 ] ];

	return op;
}

function forceTrailingNewline( line ){
	var op = getLastInsertOp( line );
		
	op.insert = op.insert + "\n";

	return line;
}

function applyAttributes( line, attributes ){
	var op = getLastInsertOp( line );

	op.attributes = op.attributes ? { ...op.attributes, ...attributes } : { ...attributes };
	
	return line;
}

function fixLinks(){
	var link = Quill.import( "formats/link" );

	class clickableLink extends link{
		static create( value ){
			var node = super.create( value );

			node.setAttribute( "href", link.sanitize(value) );
			node.setAttribute( "target", "_blank" );
			node.setAttribute( "contenteditable", "false" );

			return node;
		}
	}

	Quill.register( "formats/link", clickableLink, true );
}

export function attachRTE( scratchpad ){
	var editor;

	fixLinks();
	
	editor = new Quill(
		scratchpad,
		{
			"modules": {
				"toolbar": [
					[ { "font": [] }, "bold", "italic", "underline" ],
					[ { "color": [] }, { "background": [] } ],
					[ "link", "image", "blockquote", "code-block" ],
					[ { "header": 1 }, { "header": 2 } ],
					[ { "list": "ordered"}, { "list": "bullet" } ],
					[ "clean" ]
				]
			},
			"theme": "bubble"
		}
	);

	return editor;
}

export function deltaToLines( delta ){
	var lines = [];

	delta.eachLine( ( line, attributes ) => {
		lines.push( applyAttributes( forceTrailingNewline( line ), attributes ) );
	} );

	return lines;
}