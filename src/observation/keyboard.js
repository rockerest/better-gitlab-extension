import { deleteItem } from "../common/DB.js";
import { normalizeScratchpad, persistData } from "../common/Scratchpad.js";

function caretNode(){
	var selection = document.getSelection();

	return selection.anchorNode.parentElement;
}

export function scratchpadKeyboard( scratchpad ){
	var lastInputData = undefined;
	var lastInputType = undefined;
	var lastFocus = undefined;

	scratchpad.addEventListener( "keydown", ( event ) => {
		lastFocus = caretNode();
	} );

	scratchpad.addEventListener( "input", ( event ) => {
		lastInputData = event.data;
		lastInputType = event.inputType;
	} );

	scratchpad.addEventListener( "keyup", ( event ) => {
		let key = event.key;
		let currentFocus = caretNode();

		if( key == "Enter" && lastInputData == null ){
			normalizeScratchpad( scratchpad );
		}
		else if( currentFocus != lastFocus && lastInputType.startsWith( "deleteContent" ) && lastFocus.uuid ){
			deleteItem( lastFocus.uuid );
		}
		
		persistData( scratchpad );
	} );
}