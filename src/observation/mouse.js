export function scratchpadMouse( { personalChecklist, toggle } ){
	toggle.addEventListener( "click", () => {
		personalChecklist.classList.toggle( "closed" );
		personalChecklist.parentNode.classList.toggle( "closed" );
	} );
}