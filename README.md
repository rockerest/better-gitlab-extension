# better-gitlab-extension

Scratch a few personal itches with the GitLab UI.

- Make an editable scratchpad-type interface when on the To-Do List interface

## Install

`npm install`

## Build

`npm run build`

## Test

1. Load the unpacked extension from your install directory by following the three steps in the Chrome Extension [Getting Started - Manifest documentation](https://developer.chrome.com/docs/extensions/mv3/getstarted/#manifest)